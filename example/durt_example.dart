import 'package:durt/durt.dart';
import 'dart:typed_data';

Future<void> main() async {
  const String mnemonicLang = 'french';
  const int derivation = 2;

  print('------------------\n HD Wallet example\n------------------\n');
  final mnemonic = generateMnemonic(lang: mnemonicLang);

  // Build HdWallet object from mnemonic
  HdWallet hdWallet = HdWallet.fromMnemonic(mnemonic);

  // Get pubkey of derivation number $derivation
  String pubkey = hdWallet.getPubkey(harden(derivation));

  String message = "blabla test";

  // Sign the message with derivation number $derivation
  final signature = hdWallet.sign(message, derivation: derivation);
  bool isOK = hdWallet.verifySign(message, signature, derivation: derivation);

  print('Mnemonic: $mnemonic');
  print('Pubkey N°$derivation: $pubkey');
  print('Is signature OK ? : $isOK');

  print('\n------------------\n DEWIF example\n------------------\n');

  final dewif = Dewif();

  final dewifData =
      await dewif.generateDewif(mnemonic, 'ABCDE', lang: mnemonicLang);
  print(dewifData.dewif);

  String? decryptedDewif;
  try {
    decryptedDewif =
        dewif.mnemonicFromDewif(dewifData.dewif, 'ABCDE', lang: mnemonicLang);
    print('Unlock: $decryptedDewif');
  } on ChecksumException {
    print('Bad secret code');
  } catch (e) {
    print(e);
  }

  print(
      '\n------------------\n Cesium wallet generation example\n------------------\n');

  // Build Cesium wallet from salt + password
  final cesiumWallet =
      CesiumWallet('myCesiumID', 'myCesiumPassword'); // Cesium ID + Password

  print('Seed: ${cesiumWallet.seed}');
  print('Pubkey: ${cesiumWallet.pubkey}');

  final signatureCesium = cesiumWallet.sign(message);
  bool isOKCesium = cesiumWallet.verifySign(message, signatureCesium);

  // Is signature valid ?
  print(isOKCesium); //true

  print('\n------------------\n Dewif for Cesium wallet\n------------------\n');

  final dewifCesiumData =
      await dewif.generateCesiumDewif(cesiumWallet.seed, 'FGHIJ');

  Uint8List? decryptedCesiumDewif;
  try {
    decryptedCesiumDewif =
        dewif.cesiumSeedFromDewif(dewifCesiumData.dewif, 'FGHIJ');
    print('Unlock: $decryptedCesiumDewif');
  } on ChecksumException {
    print('Bad secret code');
  } catch (e) {
    print(e);
  }

  // Rebuild Cesium wallet, but this time from seed
  final reCesiumWallet = CesiumWallet.fromSeed(decryptedCesiumDewif!);
  print('Pubkey: ${reCesiumWallet.pubkey}');
}

import 'package:bip32_ed25519/api.dart';
import 'package:durt/src/gva/queries.dart';
import 'package:durt/src/gva/transaction.dart';
import 'package:graphql/client.dart';

import 'complex_transaction.dart';

class Gva {
  final GraphQLClient _client;
  final bool debug;
  Gva._(this._client, this.debug);

  /// Main constructor for GVA query object
  factory Gva({required String node, bool debug = false}) {
    final httpLink = HttpLink(node);

    final GraphQLClient client = GraphQLClient(
      cache: GraphQLCache(),
      link: httpLink,
    );

    return Gva._(client, debug);
  }

  /// Return the balance from a given pubkey, or 0 if null.
  Future<double> balance(String pubkey, {bool ud = false}) async {
    double balance = 0;

    final QueryOptions options = QueryOptions(
      document: gql(getBalanceQuery),
      variables: <String, dynamic>{
        'pubkey': pubkey,
      },
    );
    final QueryResult result = await _client.query(options);

    if (result.hasException) {
      printIfDebug(result.exception.toString());
      throw Exception('Unexpected error happened in graphql request');
    } else if (result.data?['balance'] != null) {
      balance = result.data!['balance']['amount'] / 100;
      if (ud) {
        double currentUd = result.data!['currentUd']['amount'] / 100;
        balance = balance / currentUd;
      }
    }

    return balance.toPrecision(2);
  }

  /// Return the username from a given pubkey.
  Future<String> getUsername(String pubkey) async {
    String username = '';

    final QueryOptions options = QueryOptions(
      document: gql(getIdQuery),
      variables: <String, dynamic>{
        'pubkey': pubkey,
      },
    );
    final QueryResult result = await _client.query(options);

    if (result.hasException) {
      printIfDebug(result.exception.toString());
      throw Exception('Unexpected error happened in graphql request');
    } else if (result.data?['idty']?['username'] != null) {
      username = result.data!['idty']['username'];
    }

    return username;
  }

  /// Return the username from a given pubkey.
  Future<double> getCurrentUd() async {
    final QueryOptions options = QueryOptions(document: gql(currentUdQuery));
    final QueryResult result = await _client.query(options);

    if (result.hasException) {
      printIfDebug(result.exception.toString());
      throw Exception('Unexpected error happened in graphql request');
    } else {
      return result.data!['currentUd']['amount'] / 100;
    }
  }

  /// Pay a given pubkey.
  /// This automatically recognizes whether it is an HD wallet or a Cesium wallet.
  /// Return 'success' string if the transaction has been successfuly proced, or the specified error
  Future<String> pay(
      {required String recipient,
      required double amount,
      String? dewif,
      String? password,
      String? mnemonic,
      String? cesiumId,
      String? cesiumPwd,
      Uint8List? cesiumSeed,
      String lang = 'english',
      int? derivation,
      String comment = '',
      bool ud = false,
      bool useMempool = false,
      bool raiseException = false}) async {
    Transaction transaction;

    if (ud) {
      final currentUd = await getCurrentUd();
      amount = amount * currentUd;
    }

    transaction = Transaction(
        recipient: recipient,
        amount: (amount.toPrecision(2) * 100).toInt(),
        dewif: dewif,
        password: password,
        mnemonic: mnemonic,
        cesiumId: cesiumId,
        cesiumPwd: cesiumPwd,
        cesiumSeed: cesiumSeed,
        derivation: derivation ?? -1,
        comment: comment,
        useMempool: useMempool,
        lang: lang,
        client: _client);

    // Execute transaction
    return await transaction.process(raiseException);
  }

  Future<String> complexPay({
    required List<String> recipients,
    required List<double> amounts,
    required double totalAmount,
    String? dewif,
    String? password,
    String? mnemonic,
    String? cesiumId,
    String? cesiumPwd,
    Uint8List? cesiumSeed,
    int? derivation,
    String lang = 'english',
    String comment = '',
    bool ud = false,
    bool useMempool = false,
    bool raiseException = false,
  }) async {
    ComplexTransaction transaction;

    if (recipients.length != amounts.length) {
      throw Exception('The size of recipients and amounts must be the same.');
    }

    if (ud) {
      final currentUd = await getCurrentUd();
      totalAmount = totalAmount * currentUd;
      amounts = amounts.map((amount) => (amount * currentUd)).toList();
    }

    transaction = ComplexTransaction(
      recipients: recipients,
      amounts: amounts
          .map((amount) => (amount.toPrecision(2) * 100).toInt())
          .toList(),
      amount: (totalAmount.toPrecision(2) * 100).toInt(),
      dewif: dewif,
      password: password,
      mnemonic: mnemonic,
      cesiumId: cesiumId,
      cesiumPwd: cesiumPwd,
      cesiumSeed: cesiumSeed,
      derivation: derivation ?? -1,
      comment: comment,
      useMempool: useMempool,
      lang: lang,
      client: _client,
    );

    // Execute transaction
    return await transaction.process(raiseException);
  }

  Future<Map<String, dynamic>?> history(String pubkey,
      [int? pageSize, String? cursor]) async {
    final QueryOptions options = QueryOptions(
      document: gql(getHistoryQuery),
      variables: <String, dynamic>{
        'pubkey': pubkey,
        'number': pageSize ?? 100,
        'cursor': cursor,
      },
    );
    final QueryResult result = await _client.query(options);

    if (result.hasException) {
      printIfDebug(result.exception.toString());
      throw Exception('Unexpected error happened in graphql request');
    } else if (result.data != null) {
      return result.data;
    }
    return null;
  }

  /// Return the current block of the node.
  Future<int> getCurrentBlock() async {
    final QueryOptions options = QueryOptions(document: gql(currentBlockQuery));
    final QueryResult result = await _client.query(options);

    if (result.hasException) {
      printIfDebug(result.exception.toString());
      throw Exception('Unexpected error happened in graphql request');
    } else {
      return result.data!['currentBlock']['number'];
    }
  }

  Future<Map<String, dynamic>> getCurrentBlockExtended() async {
    final QueryOptions options =
        QueryOptions(document: gql(currentBlockExtendedQuery));
    final QueryResult result = await _client.query(options);

    if (result.hasException) {
      printIfDebug(result.exception.toString());
      throw Exception('Unexpected error happened in graphql request');
    } else {
      return result.data!['currentBlock'];
    }
  }

  void printIfDebug(String message) {
    if (debug) {
      print(message);
    }
  }

  Future<List> getFirstUtxos(List<String> scriptsList, [int first = 10]) async {
    final QueryOptions options = QueryOptions(
      document: gql(getFirstUtxosQuery),
      variables: <String, dynamic>{
        'scripts': scriptsList,
        'first': first,
      },
    );

    final QueryResult result = await _client.query(options);

    if (result.hasException) {
      printIfDebug(result.exception.toString());
      throw Exception('Unexpected error happened in graphql request');
    } else {
      List<dynamic> utxos = List.from(result.data!['firstUtxosOfScripts']);
      return utxos;
    }
  }

  Future<Map<String, dynamic>> fetchUtxosOfScript({
    required String script,
    int pageSize = 100,
    String? cursor,
    int? amount,
  }) async {
    final QueryOptions options = QueryOptions(
      document: gql(getUtxosOfScriptQuery),
      variables: <String, dynamic>{
        'script': script,
        'pageSize': pageSize,
        'cursor': cursor,
        if (amount != null) 'amount': amount,
      },
    );

    final QueryResult result = await _client.query(options);

    if (result.hasException) {
      printIfDebug(result.exception.toString());
      throw Exception('Unexpected error happened in graphql request');
    }

    final pageInfo = result.data!['utxosOfScript']['pageInfo'];
    final edges = result.data!['utxosOfScript']['edges'];
    final endCursor = pageInfo['endCursor'];
    final hasNextPage = pageInfo['hasNextPage'];

    return {
      'utxos': edges.map((e) => e['node']).toList(),
      'endCursor': endCursor,
      'hasNextPage': hasNextPage,
    };
  }
}

extension Ex on double {
  double toPrecision(int n) => double.parse(toStringAsFixed(n));
}

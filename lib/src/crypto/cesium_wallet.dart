import 'dart:convert';
import 'package:bip32_ed25519/api.dart';
import 'package:durt/src/crypto/dewif.dart';
import 'package:fast_base58/fast_base58.dart';
import 'package:pointycastle/pointycastle.dart' as pc;

class CesiumWallet {
  final Uint8List seed;
  final SigningKey rootKey;
  final String pubkey;

  /// Root private constructor used by CesiumWallet factory.
  CesiumWallet._(
      {required this.seed, required this.rootKey, required this.pubkey});

  /// Main factory constructor which generate seed and rootKey for the given Cesium salt and password
  factory CesiumWallet(String salt, String password) {
    var scrypt = pc.KeyDerivator('scrypt');
    scrypt.init(
      pc.ScryptParameters(
        4096,
        16,
        1,
        32,
        Uint8List.fromList(salt.codeUnits),
      ),
    );
    var seed = scrypt.process(Uint8List.fromList(password.codeUnits));
    return CesiumWallet.fromSeed(seed);
  }

  /// Generate rootkey from seed
  factory CesiumWallet.fromSeed(Uint8List seed) {
    SigningKey rootKey = SigningKey(seed: seed);
    String pubkey = Base58Encode(rootKey.publicKey);
    return CesiumWallet._(seed: seed, rootKey: rootKey, pubkey: pubkey);
  }

  factory CesiumWallet.fromDewif(
    String dewif,
    String password, {
    int dewifCurrencyCode = DEWIF_CURRENCY_CODE_G1,
    int dewifVersion = DEWIF_VERSION,
    bool test = false,
  }) {
    Uint8List seed = Dewif().cesiumSeedFromDewif(dewif, password,
        dewifCurrencyCode: dewifCurrencyCode,
        dewifVersion: dewifVersion,
        test: test);
    return CesiumWallet.fromSeed(seed);
  }

  /// Provide a valid signature from given Cesium keypair for any message or text document.
  String sign(String document) {
    return base64Encode(
        rootKey.sign(document.codeUnits.toUint8List()).signature.asTypedList);
  }

  /// Verify if the given signature is valid for the given message or text document,
  /// based on given keypair
  bool verifySign(String document, String signature) {
    SignatureBase fomartedSignature = Signature(base64Decode(signature));
    return rootKey.verifyKey.verify(
        signature: fomartedSignature,
        message: document.codeUnits.toUint8List());
  }
}

import 'package:durt/durt.dart';
import 'package:test/test.dart';

void main() {
  group('wallets', () {
    test('Generate HD wallet', () async {
      const String mnemonicLang = 'french';
      final int derivation = 3;

      print('------------------\n HD Wallet Test\n------------------\n');
      final mnemonicTest = generateMnemonic(lang: mnemonicLang);
      expect(mnemonicTest.split(' ').length, 12);

      // String mnemonic =
      //     "mauve victoire victoire cupide chaleur minimal punaise utopie plumage géologie quitter chaise";
      final mnemonic =
          'guide chagrin honorer grand tablier sortir analyse lavoir circuler tarder exigence séance';
      // final mnemonic =
      //     'tongue cute mail fossil great frozen same social weasel impact brush kind';
      // final mnemonic =
      //     'acquire flat utility climb filter device liberty beyond matrix satisfy metal essence';

      HdWallet hdWallet = HdWallet.fromMnemonic(mnemonic);

      String pubkey = hdWallet.getPubkey(derivation);

      String message = "blabla test";
      final signature = hdWallet.sign(message, derivation: derivation);
      bool isOK =
          hdWallet.verifySign(message, signature, derivation: derivation);

      print('Mnemonic: $mnemonic');
      print('Pubkey N°$derivation: $pubkey');
      print('Is signature OK ? : $isOK');

      expect(pubkey, 'GauM5qWiX1uzqSdRRM51it95sMdadLnsNrRKSoMQU6xd');
      expect(isOK, true);

      final dewifWallet =
          await Dewif().generateDewif(mnemonic, 'ABCDE', lang: mnemonicLang);
      final reHdWallet =
          HdWallet.fromDewif(dewifWallet.dewif, 'ABCDE', lang: mnemonicLang);

      expect(reHdWallet.getPubkey(derivation),
          'GauM5qWiX1uzqSdRRM51it95sMdadLnsNrRKSoMQU6xd');
    });
  });
}

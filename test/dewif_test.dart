import 'package:durt/src/crypto/dewif.dart';
import 'package:test/test.dart';

void main() {
  group('wallets', () {
    test('Create dewif and unlock it', () async {
      print('\n------------------\n DEWIF Test\n------------------\n');

      Dewif dewif = Dewif();
      final mnemonicRFC =
          "crop cash unable insane eight faith inflict route frame loud box vibrant";
      final dewifDataRFC = await dewif.generateDewif(
          mnemonicRFC, 'toto titi tata',
          lang: 'english',
          dewifCurrencyCode: DEWIF_CURRENCY_CODE_G1_TEST,
          testRfc: true);
      print(dewifDataRFC.dewif);

      String? decryptedDewifRFC;
      try {
        decryptedDewifRFC = dewif.mnemonicFromDewif(
            dewifDataRFC.dewif, dewifDataRFC.password,
            lang: 'english', dewifCurrencyCode: DEWIF_CURRENCY_CODE_G1_TEST);
        print('Unlock: $decryptedDewifRFC');
      } on ChecksumException {
        print('Bad secret code');
      } catch (e) {
        print(e);
      }
      expect(
          decryptedDewifRFC?.trim(),
          equals(
              'crop cash unable insane eight faith inflict route frame loud box vibrant'));

      // Change Dewif password
      final changeDewifPassword = await dewif.changePassword(
          dewif: dewifDataRFC.dewif,
          oldPassword: dewifDataRFC.password,
          newPassword: 'NBVCX',
          lang: 'english',
          dewifCurrencyCode: DEWIF_CURRENCY_CODE_G1_TEST);

      expect(changeDewifPassword.password, 'NBVCX');

      final unlockChangeDewifPassword = dewif.mnemonicFromDewif(
          changeDewifPassword.dewif, 'NBVCX',
          lang: 'english', dewifCurrencyCode: DEWIF_CURRENCY_CODE_G1_TEST);
      expect(unlockChangeDewifPassword,
          'crop cash unable insane eight faith inflict route frame loud box vibrant');
    });
  });
}

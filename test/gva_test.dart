import 'package:test/test.dart';
import 'package:durt/durt.dart';

void main() {
  group('gva', () {
    final node = Gva(node: 'https://duniter-g1.p2p.legal/gva');
    // test('Payment from HD wallet', () async {
    //   var node = Gva(node: gvaNode);

    //   //pubkey: 89K3nUWyCrGfGHnSFMBcpyvYHu5gVu39zYhJQ9ThEUQa
    //   bool result = await node.pay(
    //       recipient: 'A6o88pwgb24Ri3N6UEGALLGhVAm7vrK8fJYHxmw91JSc',
    //       amount: 1,
    //       dewif:
    //           'AAAAAQAAAAEOAVW1ZMjWqfjZ7Bwn7UAFEqNullUojioIAiheIlE250Egmuwgl+KjO4lW8an5AExktCZopnL+ZA==',
    //       derivation: 0,
    //       password: 'ABCDE',
    //       comment: 'Hello Durt from HD wallet',
    //       useMempool: true);

    //   expect(result, true);
    // });

    // test('Payment from Cesium wallet', () async {
    //   var node = Gva(node: gvaNode);

    //   // pubkey: A6o88pwgb24Ri3N6UEGALLGhVAm7vrK8fJYHxmw91JSc
    //   bool result = await node.pay(
    //       recipient: '89K3nUWyCrGfGHnSFMBcpyvYHu5gVu39zYhJQ9ThEUQa',
    //       amount: 1,
    //       dewif:
    //           'AAAAAQAAAAEOAZBsnqo2Klgz5qHntWGH3gEyxTNE5k0bT2jiWo0ZSOMgGDbzpiTnRkCexM4rjfwSSkULy+wuzOjLOxZHqRo=',
    //       password: 'ABCDE',
    //       comment: 'Hello Durt from Cesium wallet',
    //       useMempool: true);

    //   expect(result, true);
    // });

    test('GVA identity', () async {
      final idty = await node
          .getUsername('Do99s6wQR2JLfhirPdpAERSjNbmjjECzGxHNJMiNKT3P');

      expect(idty, 'poka');
    });

    test('GVA balance', () async {
      final balance =
          await node.balance('A6o88pwgb24Ri3N6UEGALLGhVAm7vrK8fJYHxmw91JSC');

      expect(balance, 1.13);
    });

    test('GVA currentUd', () async {
      final ud = await node.getCurrentUd();
      final bool isNumber = double.tryParse(ud.toString()) != null;

      expect(isNumber, true);
    });
  });
}

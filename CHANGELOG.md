# CHANGELOG

## 0.1.9

- Upgrade deps

## 0.1.8

- Add new complex payments workflow
- Remove dead code
- Update deps

## 0.1.7

- Better exceptions catching
- Add history query
- Add currentBlock query
- Add get utxo queries
- 0.1.6
- Fix few things about dewif generation
- Can pay from mnemonic
- Can pay from Cesium wallet seed

## 0.1.5

- Give choice between Scrypt (default) and Pbkdf2 for seed generation
- Make Dewif generation async

## 0.1.4

- Now use Scrypt instead of Pbkdf2 for keys derivations, like DubpRust. Got same results.
- Add useful Dewif methods for Gecko

## 0.1.3

- Add GVA payment from both HD wallets and Cesium wallets
- Signature and verify signature now use base64 string

## 0.1.2

- Add GVA main requests: balance, idty, payments
- Simplify hdwallet sign
- Add pinenacl again due to performance

## 0.1.1+2

- Replace Pbkdf2 lib from pinenacl to pointycastle to reduce dependencies
- Remove useless packages

## 0.1.1+1

- Fix typo in readme
- Add git repository to pubsec

## 0.1.1

- Add Cesium DEWIF implementation
- Refactor a bit CesiumWallet
- Document main methods
- Improve README

## 0.1.0

- Initial version, created by poka
